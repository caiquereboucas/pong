﻿using System;

namespace Pong
{
    class Player : Rectangle
    {
        private byte id;

        public byte Id { get { return id; } }

        public int Speed { get { return 5; } }

        public DirectionY DirectionY { get; set; }

        public Player(byte id,  Color color, Ball ball, int clientWidth)
        {            
            SetSize(ball.Width, ball.Height * 3);
            SetColor(color);

            switch (id)
            {
                case 1:
                    SetPosition(-(clientWidth / 2) + (this.Width / 2), 0);
                    break;

                case 2:
                    SetPosition((clientWidth / 2) - (this.Width / 2), 0);
                    break;

                default:
                    throw new ArgumentException("Id is out of range", nameof(id));
            }

            this.id = id;
        }

        public void Move(DirectionY directionY)
        {
            switch (directionY)
            {
                case DirectionY.Down:
                    SetPosition(X, Y - Speed);
                    break;
                case DirectionY.Up:
                    SetPosition(X, Y + Speed);
                    break;
                
            } 

        }
    }

    
}
