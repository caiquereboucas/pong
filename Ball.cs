﻿using System;

namespace Pong    
{
    class Ball : Rectangle
    {
        public int Speed { get { return 3; } }

        public int Size { get { return 20; } }

        public DirectionX DirectionX { get; set; }

        public DirectionY DirectionY { get; set; }

        public Ball()
        {
            SetSize(Size, Size);
            SetPosition(0, 0);
            SetColor(Color.Yellow);
            DirectionX = DirectionX.Right;
            DirectionY = DirectionY.Up;
        }

        public void Reset()
        {
            SetPosition(0, 0);
        }

        public void InvertDirectionX()
        {
            if (DirectionX == DirectionX.Left)
                DirectionX = DirectionX.Right;
            else
                DirectionX = DirectionX.Left;
        }

        public void InvertDirectionY()
        {
            if (DirectionY == DirectionY.Up)
                DirectionY = DirectionY.Down;
            else
                DirectionY = DirectionY.Up;
        }

        public void Move(DirectionX directionX, DirectionY directionY)
        {
            switch (directionX)
            {
                case DirectionX.Left:
                    SetPosition(X - Speed, Y);
                    break;
                case DirectionX.Right:
                    SetPosition(X + Speed, Y);
                    break;
            }

            switch (directionY)
            {
                case DirectionY.Down:
                    SetPosition(X, Y - Speed);
                    break;
                case DirectionY.Up:
                    SetPosition(X, Y + Speed);
                    break;
            }

        }
    }
}
