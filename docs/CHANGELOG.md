# [2021.2.0](https://gitlab.com/caiquereboucas/pong/compare/2021.1.3...2021.2.0) (2021-01-22)


### Features

* **gitlab-ci:** add merge into develop branch and rules ([d40caee](https://gitlab.com/caiquereboucas/pong/commit/d40caeedbfa535f48206c7806ce873294be7a311))
* add develop branch into semantic-release configuration ([616df01](https://gitlab.com/caiquereboucas/pong/commit/616df0115f69abdd6b437819278c36e9b909a7d2))

## [2021.1.3](https://gitlab.com/caiquereboucas/pong/compare/2021.1.2...2021.1.3) (2021-01-22)


### Bug Fixes

* **ci:** remove echo KEY ([f2415f3](https://gitlab.com/caiquereboucas/pong/commit/f2415f3787d5ff2cd98ec22a1187848849bc89ab))

## [2021.1.2](https://gitlab.com/caiquereboucas/pong/compare/2021.1.1...2021.1.2) (2021-01-22)


### Bug Fixes

* **ci:** remove enviroment variable ([2aec020](https://gitlab.com/caiquereboucas/pong/commit/2aec020618296fbb2c5baee5af0d1bfd5aafc6d2))

## [2021.1.1](https://gitlab.com/caiquereboucas/pong/compare/2021.1.0...2021.1.1) (2021-01-22)


### Bug Fixes

* test3 ([58fb713](https://gitlab.com/caiquereboucas/pong/commit/58fb713514b8ae70b1569afd1eb7c5573ed8d204))

# [2021.1.0](https://gitlab.com/caiquereboucas/pong/compare/2021.0.0...2021.1.0) (2021-01-22)


### Bug Fixes

* add tagformat ([980668a](https://gitlab.com/caiquereboucas/pong/commit/980668aaf4f52b1ec0b5b2a6cf64dfeb111b37f1))
* version in package.json ([a1e2f3e](https://gitlab.com/caiquereboucas/pong/commit/a1e2f3e93b244fe8cb77468e34db2f253dec952f))
* **ci:** add master/develop in branch configuration ([c3d6800](https://gitlab.com/caiquereboucas/pong/commit/c3d6800e42b81dc4b5d269232d32181195c8c685))
* change localization of format-tag ([f6f6395](https://gitlab.com/caiquereboucas/pong/commit/f6f6395c67fe80d186eae37071608684895eb4e9))
* change localization of format-tag ([ec7cba4](https://gitlab.com/caiquereboucas/pong/commit/ec7cba45461e2192305daac22a8b1ec9a880f73a))
* remove tag-format from gitlab-ci.yml ([04552b2](https://gitlab.com/caiquereboucas/pong/commit/04552b21ad8f987a89823725fde71532c6ea977e))
* remove tag-format from gitlab-ci.yml ([b897373](https://gitlab.com/caiquereboucas/pong/commit/b897373f617e9316140de9f1faeef8976329b49f))
* remove tag-format from gitlab-ci.yml ([24de708](https://gitlab.com/caiquereboucas/pong/commit/24de7084fb7ff0be34a22fd0dfeab91ad8d44767))
* tag branch in .gitlab-ci.yml ([5a5f296](https://gitlab.com/caiquereboucas/pong/commit/5a5f29668f931786b9b214515d2f6f8ea9138c1a))


### Features

* add plugins getLastRelease and VerifyRelease ([ee918e2](https://gitlab.com/caiquereboucas/pong/commit/ee918e28851b152746c020fb0bd11b22c127b527))
* test 2 ([4b285c5](https://gitlab.com/caiquereboucas/pong/commit/4b285c58a60f084094a16f8ef15942e356b01c7e))
* test 2 ([9fe8134](https://gitlab.com/caiquereboucas/pong/commit/9fe81343b590f9515508621e8d71046ec5911274))

# 1.0.0 (2021-01-22)


### Bug Fixes

* version in package.json ([a1e2f3e](https://gitlab.com/caiquereboucas/pong/commit/a1e2f3e93b244fe8cb77468e34db2f253dec952f))
* **ci:** add master/develop in branch configuration ([c3d6800](https://gitlab.com/caiquereboucas/pong/commit/c3d6800e42b81dc4b5d269232d32181195c8c685))
* change localization of format-tag ([f6f6395](https://gitlab.com/caiquereboucas/pong/commit/f6f6395c67fe80d186eae37071608684895eb4e9))
* change localization of format-tag ([cc9864d](https://gitlab.com/caiquereboucas/pong/commit/cc9864dcde2fc9df33b261a907ee2bdfff6ede7e))
* change localization of format-tag ([ec7cba4](https://gitlab.com/caiquereboucas/pong/commit/ec7cba45461e2192305daac22a8b1ec9a880f73a))
* remove tag-format from gitlab-ci.yml ([04552b2](https://gitlab.com/caiquereboucas/pong/commit/04552b21ad8f987a89823725fde71532c6ea977e))
* remove tag-format from gitlab-ci.yml ([b897373](https://gitlab.com/caiquereboucas/pong/commit/b897373f617e9316140de9f1faeef8976329b49f))
* remove tag-format from gitlab-ci.yml ([24de708](https://gitlab.com/caiquereboucas/pong/commit/24de7084fb7ff0be34a22fd0dfeab91ad8d44767))
* tag branch in .gitlab-ci.yml ([5a5f296](https://gitlab.com/caiquereboucas/pong/commit/5a5f29668f931786b9b214515d2f6f8ea9138c1a))


### Features

* add plugins getLastRelease and VerifyRelease ([ee918e2](https://gitlab.com/caiquereboucas/pong/commit/ee918e28851b152746c020fb0bd11b22c127b527))
* test 2 ([4b285c5](https://gitlab.com/caiquereboucas/pong/commit/4b285c58a60f084094a16f8ef15942e356b01c7e))
* test 2 ([9fe8134](https://gitlab.com/caiquereboucas/pong/commit/9fe81343b590f9515508621e8d71046ec5911274))

# 1.0.0 (2021-01-22)


### Bug Fixes

* **ci:** add master/develop in branch configuration ([c3d6800](https://gitlab.com/caiquereboucas/pong/commit/c3d6800e42b81dc4b5d269232d32181195c8c685))
* change localization of format-tag ([f6f6395](https://gitlab.com/caiquereboucas/pong/commit/f6f6395c67fe80d186eae37071608684895eb4e9))
* change localization of format-tag ([cc9864d](https://gitlab.com/caiquereboucas/pong/commit/cc9864dcde2fc9df33b261a907ee2bdfff6ede7e))
* change localization of format-tag ([ec7cba4](https://gitlab.com/caiquereboucas/pong/commit/ec7cba45461e2192305daac22a8b1ec9a880f73a))
* remove tag-format from gitlab-ci.yml ([04552b2](https://gitlab.com/caiquereboucas/pong/commit/04552b21ad8f987a89823725fde71532c6ea977e))
* remove tag-format from gitlab-ci.yml ([b897373](https://gitlab.com/caiquereboucas/pong/commit/b897373f617e9316140de9f1faeef8976329b49f))
* remove tag-format from gitlab-ci.yml ([24de708](https://gitlab.com/caiquereboucas/pong/commit/24de7084fb7ff0be34a22fd0dfeab91ad8d44767))
* tag branch in .gitlab-ci.yml ([5a5f296](https://gitlab.com/caiquereboucas/pong/commit/5a5f29668f931786b9b214515d2f6f8ea9138c1a))


### Features

* test 2 ([4b285c5](https://gitlab.com/caiquereboucas/pong/commit/4b285c58a60f084094a16f8ef15942e356b01c7e))
* test 2 ([9fe8134](https://gitlab.com/caiquereboucas/pong/commit/9fe81343b590f9515508621e8d71046ec5911274))
