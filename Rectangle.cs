﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Pong
{
    public enum DirectionX : int { Left = -1, Right = 1 }

    public enum DirectionY : int { Down = -1, Up = 1 }

    enum Color { Red, Blue, Green, Pink, Yellow };    

    abstract class Rectangle
    {
        private int x;
        private int y;
        private int width;
        private int height;
        private float red;
        private float green;
        private float blue;

        public int X { get { return x; } }
        public int Y { get { return y; } }        
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public float Red { get { return red; } }
        public float Green { get { return green; } }
        public float Blue { get { return blue; } }

        public float Top { get { return Y + height / 2; } }
        public float Bottom { get { return Y - height / 2; } }
        public float Left { get { return X - width / 2; } }
        public float Right { get { return X + width / 2; } }

        public void SetSize(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public void SetColor(Color color)
        {
            (float, float, float) colorConverted = ColorConvert(color);
            this.red = colorConverted.Item1;
            this.green = colorConverted.Item2;
            this.blue = colorConverted.Item3;
        }

        public void SetPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public virtual void Draw()
        {
            GL.Color3(this.red, this.green, this.blue);

            GL.Begin(PrimitiveType.Quads);

            GL.Vertex2(-0.5f * this.width + this.x, -0.5f * this.height + this.y);
            GL.Vertex2(0.5f * this.width + this.x, -0.5f * this.height + this.y);
            GL.Vertex2(0.5f * this.width + this.x, 0.5f * this.height + this.y);
            GL.Vertex2(-0.5f * this.width + this.x, 0.5f * this.height + this.y);

            GL.End();
        }

        public (float, float, float) ColorConvert(Color color)
        {
            switch (color)
            {
                case Color.Red:
                    return (1.0f, 0.0f, 0.0f);

                case Color.Green:
                    return (0.0f, 1.0f, 0.0f);

                case Color.Blue:
                    return (0.0f, 0.0f, 1.0f);

                case Color.Pink:
                    return (1.0f, 0.0f, 1.0f);

                case Color.Yellow:
                    return (1.0f, 1.0f, 0.0f);

                default:
                    return (1.0f, 1.0f, 1.0f);
            }
        }

    }
}
