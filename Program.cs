﻿using System.Threading;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Pong
{
    class Program : GameWindow
    {
        Ball Ball = null;
        Player Player1 = null;
        Player Player2 = null;    

        /* Test Semantic-Release 3 */        

        public Program()
        {
            Ball = new Ball();
            Player1 = new Player(1, Color.Red, Ball, ClientSize.Width);
            Player2 = new Player(2, Color.Blue, Ball, ClientSize.Width);
        }

        public bool BallOnTop(Player player)
        {
            return Ball.Bottom < player.Top;
        }

        public bool BallOnBottom(Player player)
        {
            return Ball.Top > player.Bottom;
        }

        public bool BallOnRight(Player player)
        {
            return Ball.Left < player.Right;
        }

        public bool BallOnLeft(Player player)
        {
            return Ball.Right > player.Left;
        }

        public bool BallOnTheWall()
        {
            bool CollidedOnTop = Ball.Top >= (ClientSize.Height / 2);
            bool CollidedOnBottom = Ball.Bottom <= -(ClientSize.Height / 2);

            return CollidedOnTop || CollidedOnBottom;
        }

        public bool PlayerOnTop(Player player)
        {
            return player.Top >= (ClientSize.Height / 2);
        }

        public bool PlayerOnBottom(Player player)
        {
            return player.Bottom <= -(ClientSize.Height / 2);
        }

        public bool BallOut()
        {
            return (Ball.Left < -ClientSize.Width / 2 || Ball.Right > ClientSize.Width / 2);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            Ball.Move(Ball.DirectionX, Ball.DirectionY);

            if (BallOut())
            {
                Thread.Sleep(1000);
                Ball.Reset();
            }

            if (BallOnRight(Player1) && BallOnTop(Player1) && BallOnBottom(Player1))
            {            
                Ball.DirectionX = DirectionX.Right;

                if (Ball.Y > Player1.Y)
                    Ball.DirectionY = DirectionY.Up;
                else
                    Ball.DirectionY = DirectionY.Down;             
            }

            if (BallOnLeft(Player2) && BallOnTop(Player2) && BallOnBottom(Player2))
            {      
                Ball.DirectionX = DirectionX.Left;

                if (Ball.Y > Player2.Y)
                    Ball.DirectionY = DirectionY.Up;
                else
                    Ball.DirectionY = DirectionY.Down;              
            }

            if (BallOnTheWall())
                Ball.InvertDirectionY();

            if (Keyboard.GetState().IsKeyDown(Key.W) && !(PlayerOnTop(Player1)))
                Player1.Move(DirectionY.Up);

            if (Keyboard.GetState().IsKeyDown(Key.S) && !(PlayerOnBottom(Player1)))
                Player1.Move(DirectionY.Down);

            if (Keyboard.GetState().IsKeyDown(Key.Up) && !(PlayerOnTop(Player2)))
                Player2.Move(DirectionY.Up);

            if (Keyboard.GetState().IsKeyDown(Key.Down) && !(PlayerOnBottom(Player2)))
                Player2.Move(DirectionY.Down);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Viewport(0, 0, ClientSize.Width, ClientSize.Height);

            Matrix4 projection = Matrix4.CreateOrthographic(ClientSize.Width, ClientSize.Height, 0.0f, 1.0f);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projection);

            GL.Clear(ClearBufferMask.ColorBufferBit);

            Ball.Draw();
            Player1.Draw();
            Player2.Draw();

            SwapBuffers();
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }
    }
}
